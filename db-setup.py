#!/usr/bin/env python
# -*- coding: utf-8 -*-

import getpass
import os
import sys
from mastodon import Mastodon
from mastodon.Mastodon import MastodonMalformedEventError, MastodonNetworkError, MastodonReadTimeout, MastodonAPIError
import psycopg2
from psycopg2 import sql
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

# Returns the parameter from the specified file
def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, asking."%file_path)
        write_parameter( parameter, file_path )
        #sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

def write_parameter( parameter, file_path ):
  if not os.path.exists('config'):
    os.makedirs('config')
  print("Setting up media usage parameters...")
  print("\n")
  mastodon_db = input("Mastodon database name: ")
  mastodon_db_user = input("Mastodon database user: ")
  media_db = input("media usage database name: ")
  media_db_user = input("media usage database user: ")

  with open(file_path, "w") as text_file:
    print("mastodon_db: {}".format(mastodon_db), file=text_file)
    print("mastodon_db_user: {}".format(mastodon_db_user), file=text_file)
    print("media_db: {}".format(media_db), file=text_file)
    print("media_db_user: {}".format(media_db_user), file=text_file)

def create_table(db, db_user, table, sql):

  conn = None
  try:

    conn = psycopg2.connect(database = db, user = db_user, password = "", host = "/var/run/postgresql", port = "5432")
    cur = conn.cursor()

    print("Creating table.. "+table)
    # Create the table in PostgreSQL database
    cur.execute(sql)

    conn.commit()
    print("Table "+table+" created!")
    print("\n")

  except (Exception, psycopg2.DatabaseError) as error:

    print(error)

  finally:

    if conn is not None:

      conn.close()

#############################################################################################

# Load configuration from config file
config_filepath = "config/db_config.txt"
media_db = get_parameter("media_db", config_filepath)
media_db_user = get_parameter("media_db_user", config_filepath)

############################################################
# create database
############################################################

conn = None

try:

  conn = psycopg2.connect(dbname='postgres',
      user=media_db_user, host='',
      password='')

  conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

  cur = conn.cursor()

  print("Creating database " + media_db + ". Please wait...")

  cur.execute(sql.SQL("CREATE DATABASE {}").format(
          sql.Identifier(media_db))
      )
  print("Database " + media_db + " created!")

except (Exception, psycopg2.DatabaseError) as error:

  print(error)

finally:

  if conn is not None:

    conn.close()

#############################################################################################

try:

  conn = None
  conn = psycopg2.connect(database = media_db, user = media_db_user, password = "", host = "/var/run/postgresql", port = "5432")

except (Exception, psycopg2.DatabaseError) as error:

  print(error)
  # Load configuration from config file
  os.remove("db_config.txt")
  print("Exiting. Run db-setup again with right parameters")
  sys.exit(0)

if conn is not None:

  print("\n")
  print("Media usage parameters saved to db-config.txt!")
  print("\n")

############################################################
# Create needed tables
############################################################

db = media_db
db_user = media_db_user
table = "data"
sql = "create table " + table + " (datetime timestamptz PRIMARY KEY, attachremote bigint, attachlocal bigint, emojiremote bigint, emojilocal bigint, previewcards bigint,"
sql += "avatarremote bigint, avatarlocal bigint, headerremote bigint, headerlocal bigint, backup numeric(12,1), import bigint, settings bigint)"
create_table(db, db_user, table, sql)

#####################################

table = "evo_attachremote"
sql = "create table " + table + " (datetime timestamptz PRIMARY KEY, increase bigint)"
create_table(db, db_user, table, sql)

#####################################

table = "evo_attachlocal"
sql = "create table " + table + " (datetime timestamptz PRIMARY KEY, increase bigint)"
create_table(db, db_user, table, sql)

#####################################
table = "evo_emojiremote"
sql = "create table " + table + " (datetime timestamptz PRIMARY KEY, increase bigint)"
create_table(db, db_user, table, sql)

#####################################
table = "evo_emojilocal"
sql = "create table " + table + " (datetime timestamptz PRIMARY KEY, increase bigint)"
create_table(db, db_user, table, sql)

#####################################
table = "evo_previewcards"
sql = "create table " + table + " (datetime timestamptz PRIMARY KEY, increase bigint)"
create_table(db, db_user, table, sql)

#####################################
table = "evo_avatarremote"
sql = "create table " + table + " (datetime timestamptz PRIMARY KEY, increase bigint)"
create_table(db, db_user, table, sql)

#####################################
table = "evo_avatarlocal"
sql = "create table " + table + " (datetime timestamptz PRIMARY KEY, increase bigint)"
create_table(db, db_user, table, sql)

#####################################
table = "evo_headerremote"
sql = "create table " + table + " (datetime timestamptz PRIMARY KEY, increase bigint)"
create_table(db, db_user, table, sql)

#####################################
table = "evo_headerlocal"
sql = "create table " + table + " (datetime timestamptz PRIMARY KEY, increase bigint)"
create_table(db, db_user, table, sql)

#####################################
table = "evo_backup"
sql = "create table " + table + " (datetime timestamptz PRIMARY KEY, increase bigint)"
create_table(db, db_user, table, sql)

#####################################
table = "evo_import"
sql = "create table " + table + " (datetime timestamptz PRIMARY KEY, increase bigint)"
create_table(db, db_user, table, sql)

#####################################
table = "evo_settings"
sql = "create table " + table + " (datetime timestamptz PRIMARY KEY, increase bigint)"
create_table(db, db_user, table, sql)

#####################################

print("Done!")
print("Now you can run setup.py!")
print("\n")
