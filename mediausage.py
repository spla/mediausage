#!/usr/bin/env python
# -*- coding: utf-8 -*-

from mastodon import Mastodon
import psycopg2
import subprocess
import os
import sys
import os.path        # For checking whether secrets file exists
import re
import math

def size(size_bytes):
    if size_bytes == 0:
        return "0B"
    size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
    i = int(math.floor(math.log(size_bytes, 1024)))
    p = math.pow(1024, i)
    s = round(size_bytes / p, 2)
    return "%s %s" % (s, size_name[i])

###############################################################################
# INITIALISATION
###############################################################################

# Returns the parameter from the specified file
def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, exiting."%file_path)
        sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

# Load secrets from secrets file
secrets_filepath = "secrets/secrets.txt"
uc_client_id     = get_parameter("uc_client_id",     secrets_filepath)
uc_client_secret = get_parameter("uc_client_secret", secrets_filepath)
uc_access_token  = get_parameter("uc_access_token",  secrets_filepath)

# Load configuration from config file
config_filepath = "config/config.txt"
mastodon_hostname = get_parameter("mastodon_hostname", config_filepath) # E.g., mastodon.social

# Initialise Mastodon API
mastodon = Mastodon(
    client_id = uc_client_id,
    client_secret = uc_client_secret,
    access_token = uc_access_token,
    api_base_url = 'https://' + mastodon_hostname,
)

# Initialise access headers
headers={ 'Authorization': 'Bearer %s'%uc_access_token }

###############################################################################

local_users_id = []
remote_attach_size = 0
local_attach_size = 0
remote_emojis_size = 0
local_emojis_size = 0
total_preview_cards_size = 0
remote_avatar_size = 0
local_avatar_size = 0
remote_header_size = 0
local_header_size = 0
backup_size = 0
imports_size = 0
settings_size = 0

try:

    conn = None
    conn = psycopg2.connect(database = "mastodon_production", user = "mastodon", password = "", host = "/var/run/postgresql", port = "5432")

    cur = conn.cursor()

    ## Attachments
    cur.execute("select sum(file_file_size) from media_attachments where account_id not in (select account_id from users order by account_id asc)")
    rowattach_remote = cur.fetchone()
    remote_attach_size = rowattach_remote[0]

    cur.execute("select sum(file_file_size) from media_attachments where account_id in (select account_id from users order by account_id asc)")
    rowattach_local = cur.fetchone()
    local_attach_size = rowattach_local[0]

    ## Avatars
    cur.execute("select sum(avatar_file_size) from accounts where id not in (select account_id from users order by account_id asc)")
    rowavatar_remote = cur.fetchone()
    remote_avatar_size = rowavatar_remote[0]

    cur.execute("select sum(avatar_file_size) from accounts where id in (select account_id from users order by account_id asc)")
    rowavatar_local = cur.fetchone()
    local_avatar_size = rowavatar_local[0]

    ## Headers
    cur.execute("select sum(header_file_size) from accounts where id not in (select account_id from users order by account_id asc)")
    rowheader = cur.fetchone()
    remote_header_size = rowheader[0]

    cur.execute("select sum(header_file_size) from accounts where id in (select account_id from users order by account_id asc)")
    rowheader_local = cur.fetchone()
    local_header_size = rowheader_local[0]

    ## Emojis
    cur.execute("select sum(image_file_size) from custom_emojis where domain is not null")
    row = cur.fetchone()
    remote_emojis_size = row[0]

    cur.execute("select sum(image_file_size) from custom_emojis where domain is null")
    rowemoji_local = cur.fetchone()
    local_emojis_size = rowemoji_local[0]

    ## Preview cards
    cur.execute("select sum(image_file_size) from preview_cards")
    rowpreview = cur.fetchone()
    total_preview_cards_size = rowpreview[0]

    ## Backups
    cur.execute("select sum(dump_file_size) from backups")
    row = cur.fetchone()
    backup_size = row[0]

    ## Imports
    cur.execute("select sum(data_file_size) from imports")
    rowimports = cur.fetchone()
    imports_size = rowimports[0]

    ## Settings
    cur.execute("select sum(file_file_size) from site_uploads")
    rowsettings = cur.fetchone()
    settings_size = rowsettings[0]

    cur.close()

except (Exception, psycopg2.DatabaseError) as error:
    print (error)
finally:
    if conn is not None:
        conn.close()

toot_text = "https://" + mastodon_hostname + " media usage: " + "\n"
toot_text +='\n'
toot_text += "Attachments: " + str(size(remote_attach_size)) + " (local: " + str(size(local_attach_size)) + ")\n"
toot_text += "Emojis: "+ str(size(remote_emojis_size)) + " (local: "+ str(size(local_emojis_size)) + ")\n"
toot_text += "Preview cards: "+ str(size(total_preview_cards_size)) + "\n"
toot_text += "Avatars: " + str(size(remote_avatar_size)) + " (local: " + str(size(local_avatar_size)) + ")\n"
toot_text += "Headers: " + str(size(remote_header_size)) + " (local: " + str(size(local_header_size)) + ")\n"
toot_text += "Backups: " + str(size(float(backup_size))) + "\n"
toot_text += "Imports: " + str(size(imports_size)) + "\n"
toot_text += "Settings: "+ str(size(settings_size)) + "\n"
toot_text += "\n"

print("Tooting...")
print(toot_text)
toot_id = mastodon.status_post(toot_text, in_reply_to_id=None,)
