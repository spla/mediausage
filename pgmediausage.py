#!/usr/bin/env python
# -*- coding: utf-8 -*-

from mastodon import Mastodon
import psycopg2
import subprocess
import os
import sys
import os.path        # For checking whether secrets file exists
import re
import math
import datetime

def size(size_bytes):
   if size_bytes == 0:
       return "0B"
   size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
   i = int(math.floor(math.log(size_bytes, 1024)))
   p = math.pow(1024, i)
   s = round(size_bytes / p, 2)
   return "%s %s" % (s, size_name[i])

def write_evo(sqlquery, table):
    try:

        conn = None
        conn = psycopg2.connect(database = media_db, user = media_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()
        cur.execute(sqlquery)

        rows = cur.fetchall()
        for row in rows:

            inserta_line = "INSERT INTO " + table + "(datetime, increase) VALUES(%s,%s) ON CONFLICT DO NOTHING"

            cur.execute(inserta_line, (row[0], row[1]))

        conn.commit()
        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:

        print (error)

    finally:

        if conn is not None:

            conn.close()

###############################################################################
# INITIALISATION
###############################################################################

# Returns the parameter from the specified file
def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, exiting."%file_path)
        sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

# Load secrets from secrets file
secrets_filepath = "secrets/secrets.txt"
uc_client_id     = get_parameter("uc_client_id",     secrets_filepath)
uc_client_secret = get_parameter("uc_client_secret", secrets_filepath)
uc_access_token  = get_parameter("uc_access_token",  secrets_filepath)

# Load configuration from config file
config_filepath = "config/config.txt"
mastodon_hostname = get_parameter("mastodon_hostname", config_filepath) # E.g., mastodon.social
mastodon_root_dir = get_parameter("mastodon_root_dir", config_filepath) # E.g., /home/mastodon/live

config_filepath = "config/db_config.txt"
mastodon_db = get_parameter("mastodon_db", config_filepath)
mastodon_db_user = get_parameter("mastodon_db_user", config_filepath)
media_db = get_parameter("media_db", config_filepath)
media_db_user = get_parameter("media_db_user", config_filepath)

# Initialise Mastodon API
mastodon = Mastodon(
    client_id = uc_client_id,
    client_secret = uc_client_secret,
    access_token = uc_access_token,
    api_base_url = 'https://' + mastodon_hostname,
)

# Initialise access headers
headers={ 'Authorization': 'Bearer %s'%uc_access_token }

###############################################################################

now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
hour_check = datetime.datetime.now()

local_users_id = []
remote_attach_size = 0
local_attach_size = 0
remote_emojis_size = 0
local_emojis_size = 0
total_preview_cards_size = 0
remote_avatar_size = 0
local_avatar_size = 0
remote_header_size = 0
local_header_size = 0
backup_size = 0
imports_size = 0
settings_size = 0

try:

    conn = None
    conn = psycopg2.connect(database = mastodon_db, user = mastodon_db_user, password = "", host = "/var/run/postgresql", port = "5432")

    cur = conn.cursor()

    ## Attachments
    cur.execute("select sum(file_file_size) from media_attachments where account_id not in (select account_id from users order by account_id asc)")
    rowattach_remote = cur.fetchone()
    remote_attach_size = rowattach_remote[0]

    cur.execute("select sum(file_file_size) from media_attachments where account_id in (select account_id from users order by account_id asc)")
    rowattach_local = cur.fetchone()
    local_attach_size = rowattach_local[0]

    ## Avatars
    cur.execute("select sum(avatar_file_size) from accounts where id not in (select account_id from users order by account_id asc)")
    rowavatar_remote = cur.fetchone()
    remote_avatar_size = rowavatar_remote[0]

    cur.execute("select sum(avatar_file_size) from accounts where id in (select account_id from users order by account_id asc)")
    rowavatar_local = cur.fetchone()
    local_avatar_size = rowavatar_local[0]

    ## Headers
    cur.execute("select sum(header_file_size) from accounts where id not in (select account_id from users order by account_id asc)")
    rowheader = cur.fetchone()
    remote_header_size = rowheader[0]

    cur.execute("select sum(header_file_size) from accounts where id in (select account_id from users order by account_id asc)")
    rowheader_local = cur.fetchone()
    local_header_size = rowheader_local[0]

    ## Emojis
    cur.execute("select sum(image_file_size) from custom_emojis where domain is not null")
    row = cur.fetchone()
    remote_emojis_size = row[0]

    cur.execute("select sum(image_file_size) from custom_emojis where domain is null")
    rowemoji_local = cur.fetchone()
    local_emojis_size = rowemoji_local[0]

    ## Preview cards
    cur.execute("select sum(image_file_size) from preview_cards")
    rowpreview = cur.fetchone()
    total_preview_cards_size = rowpreview[0]

    ## Backups
    cur.execute("select sum(dump_file_size) from backups")
    row = cur.fetchone()
    if row[0] != None:
        backup_size = row[0]
    else:
        backup_size = 0

    ## Imports
    cur.execute("select sum(data_file_size) from imports")
    rowimports = cur.fetchone()
    imports_size = rowimports[0]

    ## Settings
    cur.execute("select sum(file_file_size) from site_uploads")
    rowsettings = cur.fetchone()
    settings_size = rowsettings[0]

    cur.close()

except (Exception, psycopg2.DatabaseError) as error:

    print (error)

finally:

    if conn is not None:

        conn.close()

######################################################
# write data to postgresql's media usage database
######################################################

backup_size = float(backup_size)

sqlquery = "INSERT INTO data(datetime, attachremote, attachlocal, emojiremote, emojilocal, previewcards, avatarremote, avatarlocal, headerremote, headerlocal, backup, import, settings) "
sqlquery += "VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

try:

    conn = None
    conn = psycopg2.connect(database = media_db, user = media_db_user, password = "", host = "/var/run/postgresql", port = "5432")

    cur = conn.cursor()
    cur.execute(sqlquery, (now, remote_attach_size, local_attach_size, remote_emojis_size, local_emojis_size, total_preview_cards_size, remote_avatar_size, local_avatar_size, remote_header_size, local_header_size, backup_size, imports_size, settings_size))

    conn.commit()
    cur.close()

except (Exception, psycopg2.DatabaseError) as error:

    print (error)

finally:

    if conn is not None:
        conn.close()

######################################################
# get evolution
######################################################

sqlquery = "select datetime, attachremote - lag(attachremote) over (order by datetime) as increase from data"
table = "evo_attachremote"
write_evo(sqlquery, table)

sqlquery = "select datetime, attachlocal - lag(attachlocal) over (order by datetime) as increase from data"
table = "evo_attachlocal"
write_evo(sqlquery, table)

sqlquery = "select datetime, emojiremote - lag(emojiremote) over (order by datetime) as increase from data"
table = "evo_emojiremote"
write_evo(sqlquery, table)

sqlquery = "select datetime, emojilocal - lag(emojilocal) over (order by datetime) as increase from data"
table = "evo_emojilocal"
write_evo(sqlquery, table)

sqlquery = "select datetime, previewcards - lag(previewcards) over (order by datetime) as increase from data"
table = "evo_previewcards"
write_evo(sqlquery, table)

sqlquery = "select datetime, avatarremote - lag(avatarremote) over (order by datetime) as increase from data"
table = "evo_avatarremote"
write_evo(sqlquery, table)

sqlquery = "select datetime, avatarlocal - lag(avatarlocal) over (order by datetime) as increase from data"
table = "evo_avatarlocal"
write_evo(sqlquery, table)

sqlquery = "select datetime, headerremote - lag(headerremote) over (order by datetime) as increase from data"
table = "evo_headerremote"
write_evo(sqlquery, table)

sqlquery = "select datetime, headerlocal - lag(headerlocal) over (order by datetime) as increase from data"
table = "evo_headerlocal"
write_evo(sqlquery, table)

sqlquery = "select datetime, backup - lag(backup) over (order by datetime) as increase from data"
table = "evo_backup"
write_evo(sqlquery, table)

sqlquery = "select datetime, import - lag(import) over (order by datetime) as increase from data"
table = "evo_import"
write_evo(sqlquery, table)

sqlquery = "select datetime, settings - lag(settings) over (order by datetime) as increase from data"
table = "evo_settings"
write_evo(sqlquery, table)

######################################################
# Tooting
######################################################

total_size = 0
if hour_check.hour == 12:

    try:

        conn = None
        conn = psycopg2.connect(database = media_db, user = media_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()
        cur.execute("select * from data where datetime > now() - interval '24 hours' limit 1")

        row = cur.fetchall()

        remote_attach_before = row[0][1]
        local_attach_before = row[0][2]
        remote_emoji_before = row[0][3]
        local_emoji_before = row[0][4]
        previewcards_before = row[0][5]
        remote_avatar_before = row[0][6]
        local_avatar_before = row[0][7]
        remote_header_before = row[0][8]
        local_header_before = row[0][9]
        backup_before = row[0][10]
        imports_before = row[0][11]
        settings_before = row[0][12]

        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:

        print (error)

    finally:

        if conn is not None:

            conn.close()

    total_size = remote_attach_size+local_attach_size+remote_emojis_size+local_emojis_size+total_preview_cards_size+remote_avatar_size+local_avatar_size+remote_header_size+local_header_size
    total_size = total_size + float(backup_size) + imports_size + settings_size

    total_before = remote_attach_before + local_attach_before + remote_emoji_before+ local_emoji_before+previewcards_before+remote_avatar_before+local_avatar_before+remote_header_before
    total_before = total_before + local_header_before+float(backup_before)+imports_before+settings_before

    toot_text = "https://" + mastodon_hostname + " media usage: " + "\n"
    toot_text +='\n'
    toot_text += "Attachments: " + str(size(remote_attach_size))+" ("+str(size(remote_attach_before))+")" + ", local: " + str(size(local_attach_size)) +" ("+str(size(local_attach_before))+") " + "\n"
    toot_text += "Custom emoji: "+ str(size(remote_emojis_size)) +" ("+str(size(remote_emoji_before))+")" + ", local: "+ str(size(local_emojis_size)) + " ("+str(size(local_emoji_before))+") " + "\n"
    toot_text += "Preview cards: "+ str(size(total_preview_cards_size)) +" ("+str(size(previewcards_before))+") "+ "\n"
    toot_text += "Avatars: " + str(size(remote_avatar_size)) + " ("+str(size(remote_avatar_before))+")" + ", local: " + str(size(local_avatar_size)) +" ("+str(size(local_avatar_before))+") " + "\n"
    toot_text += "Headers: " + str(size(remote_header_size)) +" ("+str(size(remote_header_before))+")"+ ", local: " + str(size(local_header_size)) +" ("+str(size(local_header_before))+") " + "\n"
    toot_text += "Backups: " + str(size(float(backup_size))) +" ("+str(size(float(backup_before)))+") "+ "\n"
    toot_text += "Imports: " + str(size(imports_size)) +" ("+str(size(imports_before))+") " + "\n"
    toot_text += "Settings: "+ str(size(settings_size)) +" ("+str(size(settings_before))+") "+ "\n"
    toot_text += "\n"
    toot_text += "Total: " + str(size(total_size)) +" ("+str(size(total_before))+") " + "\n"
    toot_text += "\n"
    toot_text += "https://grafana.mastodont.cat/d/YApiNv3Wz/us-media-espai-ocupat-en-disc-dur?orgId=1&kiosk" + ")\n"
    toot_text += "\n"
    toot_text += "#Mastodon #mediausage"

    print("Tooting...")
    print(toot_text)

    toot_id = mastodon.status_post(toot_text, in_reply_to_id=None,)

else:

     total_size = remote_attach_size+local_attach_size+remote_emojis_size+local_emojis_size+total_preview_cards_size+remote_avatar_size+local_avatar_size+remote_header_size+local_header_size
     total_size = total_size + float(backup_size) + imports_size + settings_size
     total_size = size(total_size)
     toot_text = "https://" + mastodon_hostname + " media usage: " + "\n"
     toot_text +='\n'
     toot_text += "Attachments: " + str(size(remote_attach_size)) + " (local: " + str(size(local_attach_size)) + ")\n"
     toot_text += "Custom emoji: "+ str(size(remote_emojis_size)) + " (local: "+ str(size(local_emojis_size)) + ")\n"
     toot_text += "Preview cards: "+ str(size(total_preview_cards_size)) + "\n"
     toot_text += "Avatars: " + str(size(remote_avatar_size)) + " (local: " + str(size(local_avatar_size)) + ")\n"
     toot_text += "Headers: " + str(size(remote_header_size)) + " (local: " + str(size(local_header_size)) + ")\n"
     toot_text += "Backups: " + str(size(float(backup_size))) + "\n"
     toot_text += "Imports: " + str(size(imports_size)) + "\n"
     toot_text += "Settings: "+ str(size(settings_size)) + "\n"
     toot_text += "\n"
     toot_text += "Total: " + str(total_size) + "\n"
     toot_text += "\n"
     toot_text += "https://grafana.mastodont.cat/d/YApiNv3Wz/us-media-espai-ocupat-en-disc-dur?orgId=1&kiosk" + ")\n"
     toot_text += "\n"
     toot_text += "#Mastodon #mediausage"

     print("Tooting...")
     print(toot_text)
