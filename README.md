# mediausage
Bot for Mastodon to toot your server media usage.
Get same data that Mastodon's tootctl media usage tool but way faster and accurate. Toot the results to your Mastodon server.
Use your Grafana instance to track your Mastodon server media usage evolution.

### Dependencies

-   *Python3*
-   Mastodon server admin account.
-   Existing user/bot account. 
-   Postgresql server.  

### Usage:

Within Python Virtual Environment:

1. Run 'python db-setup.py' to create needed Postgresql database and tables. Can be used to feed data to your Grafana instance. 

2. Run 'python setup.py' to set up Mastodon hostname and mastodon user account parameters.

3. Run 'python pgmediausage.py' to toot your Mastodon's server media usage and write collected data to newly created Postgresql database.

4. Run 'python mediausage.py' if you only want to toot your media usage without writing results to Postgresql database.

5. Use your favourite scheduling method to set pgmediausage.py or mediausage.py to run regularly. 

Note: install all needed packages with 'pip install package' or use 'pip install -r requirements.txt' to install them. 


